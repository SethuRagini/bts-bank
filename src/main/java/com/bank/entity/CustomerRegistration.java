package com.bank.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;



import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerRegistration {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String name;
	private  String email;
	private long phonenumber;
	private String dob;
	private long aadharcardnumber;
	private String Customerid;
	private String 	password;
	private String AccountNumber;
	@OneToOne
	@Cascade(CascadeType.ALL)
	private Address address;
}