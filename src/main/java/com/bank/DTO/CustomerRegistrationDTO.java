package com.bank.DTO;





import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerRegistrationDTO {
	@NotBlank(message = "Name required")
	private String name;

	@Email(message = "Invalid Email")
	@NotBlank(message = "Email Required")
	private String email;

	private long phonenumber;
	private String dob;

	private long aadharcardnumber;
	private String password;
	
	
	private AddressDTO address;
}
