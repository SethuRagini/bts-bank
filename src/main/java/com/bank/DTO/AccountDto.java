package com.bank.DTO;

public record AccountDto(int accountId,

		long accountNumber,

		String accounttype,

		double balance,

		int userId) {

}
