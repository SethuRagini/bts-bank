package com.bank.exception;

public class GlobalErrorCode {
	public static final String ERROR_RESOURCE_NOT_FOUND = "404";
	public static final String ERROR_RESOURCE_CONFLICT_EXISTS = "409";
	public static final String ERROR_UNAUTHOURIZED_USER = "401";
	public static final String ERROR_INSUFFICIENT_FUND = "400";

	private GlobalErrorCode() {
		super();
	}
}
