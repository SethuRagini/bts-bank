package com.bank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.DTO.CustomerRegistrationDTO;
import com.bank.DTO.ResponseDto;
import com.bank.service.ServiceInterface;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/customers")
public class RegistrationController {
 
	public final ServiceInterface registrationservice;
	public RegistrationController(ServiceInterface registrationservice) {
		super();
		this.registrationservice = registrationservice;
	}
 
	
	@PostMapping("/register")
	public ResponseEntity<ResponseDto> registerCustomer(@RequestBody @Valid CustomerRegistrationDTO RegistrationDto) {
 
		return new ResponseEntity<>(registrationservice.registerCustomer(RegistrationDto), HttpStatus.CREATED);
 
	}
}
