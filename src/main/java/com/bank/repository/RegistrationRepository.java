package com.bank.repository;



import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bank.entity.CustomerRegistration;
 
public interface RegistrationRepository extends JpaRepository<CustomerRegistration, Long> {

	Optional<CustomerRegistration> findByEmail(String email);

	//void save(Optional<CustomerRegistration> customer);

	//void save(Optional<CustomerRegistration> customer);

	// save(Optional<CustomerRegistration> customer);

	//boolean existsByEmail(String email);
	
   
}
