package com.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bank.entity.Address;

public interface AddresRepo extends JpaRepository<Address, Long> {

}
