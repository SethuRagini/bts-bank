package com.bank.service;

import com.bank.DTO.CustomerRegistrationDTO;
import com.bank.DTO.ResponseDto;

public interface ServiceInterface {

	ResponseDto registerCustomer(CustomerRegistrationDTO RegistrationDto);

}
