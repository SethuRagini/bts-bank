package com.bank.serviceimp;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.bank.DTO.AddressDTO;
import com.bank.DTO.CustomerRegistrationDTO;
import com.bank.DTO.ResponseDto;
import com.bank.entity.Address;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.ResourceNotFound;
import com.bank.repository.AddresRepo;
import com.bank.repository.RegistrationRepository;
import com.bank.service.ServiceInterface;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImp implements ServiceInterface {
 
	private final RegistrationRepository registrationRepo ;
	private final AddresRepo addresRepo;
	
	
	
	
	@Override
	public ResponseDto registerCustomer(CustomerRegistrationDTO RegistrationDto) {
		Optional <CustomerRegistration> customer = registrationRepo.findByEmail(RegistrationDto.getEmail());
		if(customer.isPresent()) {
			throw new ResourceNotFound("customer already registred");
		}
		
		CustomerRegistration customer1=new CustomerRegistration();
		BeanUtils.copyProperties(RegistrationDto, customer1);
		customer1.setAddress(getAddress(RegistrationDto.getAddress()));
//		customer1.builder().address(getAddress(RegistrationDto.getAddress()));
		registrationRepo.save(customer1);
		return ResponseDto.builder().code(200l).message("Registered successfully").build();
 
	}
	private Address getAddress(AddressDTO addressDto) {
		Address address=  Address.builder().city(addressDto.getCity()).country(addressDto.getCountry()).lane(addressDto.getLane())
				.state(addressDto.getState()).pincode(addressDto.getPincode()).build();
		return address;
		
		
	}
 
}
