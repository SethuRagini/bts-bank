package com.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtsBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(BtsBankApplication.class, args);
	}

}
